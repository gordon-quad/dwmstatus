/** ********************************************************************
 * DWM STATUS by <clement@6pi.fr>
 *
 * Compile with:
 * gcc -Wall -pedantic -std=c99 -lX11 -lasound dwmstatus.c
 *
 **/

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <wchar.h>
#include <unistd.h>
#include <time.h>
#include <X11/Xlib.h>
#include <math.h>

#include <limits.h>
#include <linux/wireless.h>

#include <err.h>
#include <sys/ioctl.h>

/* Alsa */
#include <alsa/asoundlib.h>
#include <alsa/mixer.h>
/* Oss (not working, using popen + ossmix)
#include <linux/soundcard.h>
*/

#define CPU_NBR 4
#define BAR_HEIGHT 24
#define BAT_NOW_FILE "/sys/class/power_supply/BAT0/charge_now"
#define BAT_FULL_FILE "/sys/class/power_supply/BAT0/charge_full"
#define BAT_STATUS_FILE "/sys/class/power_supply/BAT0/status"

#define NAME_SENSOR_FILE "/sys/class/hwmon/hwmon%d/name"
#define SENSOR_NAME "coretemp"
#define TEMP1_SENSOR_FILE "/sys/class/hwmon/hwmon%d/temp2_input"
#define TEMP2_SENSOR_FILE "/sys/class/hwmon/hwmon%d/temp3_input"
#define MEMINFO_FILE "/proc/meminfo"
#define WLAN_INTERFACE "wlan0"

#define CHARGER_ALARM_TICKS 20

#define min(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a < _b ? _a : _b; })

int   get_battery();
int   get_battery_status();
int   get_mem_percent();
void  get_cpu_usage(int *cpu_percent);
char* get_date_time();
float get_freq(char *file);
int   get_temperature(char *file);
int   get_volume();
void  set_status(Display *dpy, char *str);
int   get_wifi_percent();
char* get_wifi_essid();
int   find_sensor();

char* v_bar(int percent, int w, int h, char* fg_color, char* bg_color);
char* h_bar(int percent, int w, int h, char* fg_color, char* bg_color);
int h2_bar(char *string, size_t size, int percent, int w, int h, char *fg_color, char *bg_color);
int h_bar_bordered(char *string, size_t size, int percent, int w, int h, char *fg_color, char *bg_color, char *border_color);
int get_battery_bar(char *string, size_t size, int w, int h);
void percent_color_generic(char* string, int percent, int invert);

int charger_alarm_indicator = 0;
int charger_status_last = 0;


/* *******************************************************************
 * MAIN
 ******************************************************************* */

int
main(void)
{
    const int MSIZE = 2048;
    Display *dpy;
    char *status;

    int cpu_percent[CPU_NBR];
    char *datetime;
    int temp1, temp2, vol, wifi;
    char *cpu_bar[CPU_NBR];
    char *wifi_essid;
    char *wifi_bar;
    char wifi_color[8];

    int mem_percent;
    char *mem_bar;
    char mem_color[8];

    char *fg_color = "#EEEEEE";
    char cpu_color[8];

    char bat0[256];
    int tempn;
    char temp1_fn[256];
    char temp2_fn[256];

    if (!(dpy = XOpenDisplay(NULL))) {
        fprintf(stderr, "Cannot open display.\n");
        return EXIT_FAILURE;
    }

    status = (char*) malloc(sizeof(char)*MSIZE);
    if(!status)
        return EXIT_FAILURE;

    tempn = find_sensor();
    sprintf(temp1_fn, TEMP1_SENSOR_FILE, tempn);
    sprintf(temp2_fn, TEMP2_SENSOR_FILE, tempn);

    while(1)
    {
        mem_percent = get_mem_percent();
        percent_color_generic(mem_color, mem_percent, 1);
        mem_bar = h_bar(mem_percent, 20, 14,  mem_color, "#444444");
        temp1 = get_temperature(temp1_fn);
        temp2 = get_temperature(temp2_fn);
        datetime = get_date_time();
        get_battery_bar(bat0, 512, 30, 14);
        vol = get_volume();
        get_cpu_usage(cpu_percent);
        wifi = get_wifi_percent();
        wifi_essid = get_wifi_essid();
        percent_color_generic(wifi_color, wifi, 1);
        wifi_bar = v_bar(wifi, 4, 14, wifi_color, "#444444");
        for(int i = 0; i < CPU_NBR; ++i)
        {
            percent_color_generic(cpu_color, cpu_percent[i], 1);
            cpu_bar[i] = v_bar(cpu_percent[i], 4, 14, cpu_color, "#444444");
        }

        int ret = snprintf(
            status,
            MSIZE,
            "^c%s^ %s ;^c%s^ [VOL %3d%%] [CPU^f3^%s^f6^%s^f6^^c%s^] [Temp %3d°C/%3d°C] [MEM^f3^%s^f20^^c%s^] [Wifi %s^f3^%s^f5^^c%s^] [Bat%s]",
            fg_color,
            datetime,
            fg_color,
            vol,
            cpu_bar[0],
            cpu_bar[2],
            fg_color,
            temp1,
            temp2,
            mem_bar,
            fg_color,
            wifi_essid,
            wifi_bar,
            fg_color,
            bat0
            );
        if(ret >= MSIZE)
            fprintf(stderr, "error: buffer too small %d/%d\n", MSIZE, ret);

        for(int i = 0; i < CPU_NBR; ++i)
            free(cpu_bar[i]);

        free(mem_bar);

        set_status(dpy, status);
        usleep(500000);
    }

    free(status);
    XCloseDisplay(dpy);

    return 0;
}

/* *******************************************************************
 * FUNCTIONS
 ******************************************************************* */

char* v_bar(int percent, int w, int h, char* fg_color, char* bg_color)
{
    char *value;
    if((value = (char*) malloc(sizeof(char)*128)) == NULL)
    {
        fprintf(stderr, "Cannot allocate memory for buf.\n");
        exit(1);
    }
    char* format = "^c%s^^r0,%d,%d,%d^^c%s^^r0,%d,%d,%d^";

    int bar_height = (percent*h)/100;
    int y = (BAR_HEIGHT - h)/2;
    snprintf(value, 128, format, bg_color, y, w, h, fg_color, y + h-bar_height, w, bar_height);
    return value;
}

char* h_bar(int percent, int w, int h, char* fg_color, char* bg_color)
{
    char *value;
    if((value = (char*) malloc(sizeof(char)*128)) == NULL)
    {
        fprintf(stderr, "Cannot allocate memory for buf.\n");
        exit(1);
    }
    char* format = "^c%s^^r0,%d,%d,%d^^c%s^^r0,%d,%d,%d^";

    int bar_width = (percent*w)/100;
    int y = (BAR_HEIGHT - h)/2;
    snprintf(value, 128, format, bg_color, y, w, h, fg_color, y, bar_width, h);
    return value;
}

int h_bar2(char *string, size_t size, int percent, int w, int h, char *fg_color, char *bg_color)
{
    char *format = "^c%s^^r0,%d,%d,%d^^c%s^^r%d,%d,%d,%d^";
    int bar_width = ceil((w)*(percent/100.0));

    int y = (BAR_HEIGHT - h)/2;
    return snprintf(string, size, format, fg_color, y, bar_width, h, bg_color, bar_width, y, w - bar_width, h);
}

int h_bar_bordered(char *string, size_t size, int percent, int w, int h, char *fg_color, char *bg_color, char *border_color)
{
    char tmp[128];
    h_bar2(tmp, 128, percent, w - 3, h - 2, fg_color, bg_color);
    int y = (BAR_HEIGHT - h)/2;
    char *format = "^c%s^^r0,%d,%d,%d^^f1^%s";
    return snprintf(string, size, format, border_color, y, w, h, tmp);
}

void
set_status(Display *dpy, char *str)
{
    XStoreName(dpy, DefaultRootWindow(dpy), str);
    XSync(dpy, False);
}

void percent_color_generic(char* string, int percent, int invert)
{
    char *format = "#%X0%X000";
    int a = (percent*15)/100;
    int b = 15 - a;
    if(!invert) {
        snprintf(string, 8, format, b, a);
    }
    else {
        snprintf(string, 8, format, a, b);
    }
}

void percent_color(char* string, int percent)
{
    percent_color_generic(string, percent, 0);
}

int get_battery_bar(char *string, size_t size, int w, int h)
{
    int percent = get_battery();
    int current_charger_status = get_battery_status();

    char *bg_color = "#444444";
    char *alarm_color = "#FF0000";
    char *border_color = "#EEEEEE";
    char fg_color[8];

    if((current_charger_status == 0) && (charger_status_last == 1))
    {
        charger_alarm_indicator = CHARGER_ALARM_TICKS;
    }
    charger_status_last = current_charger_status;

    if(charger_alarm_indicator > 0)
    {
        memcpy(fg_color, alarm_color, 8);
        charger_alarm_indicator--;
    }
    else if(current_charger_status)
        memcpy(fg_color, border_color, 8);
    else
        percent_color(fg_color, percent);

    char tmp[128];
    h_bar_bordered(tmp, 128, percent, w, h, fg_color, bg_color, border_color);

    char *format = "%s^c%s^^f%d^^r0,%d,%d,%d^^f%d^";
    int y = (BAR_HEIGHT - 5)/2;
    return snprintf(string, size, format, tmp, border_color, w - 2, y, 2, 5, 2);
}

int
get_battery()
{
    FILE *fd;
    int energy_now;

    static int energy_full = -1;
    if(energy_full == -1)
    {
        fd = fopen(BAT_FULL_FILE, "r");
        if(fd == NULL) {
            fprintf(stderr, "Error opening energy_full.\n");
            return -1;
        }
        fscanf(fd, "%d", &energy_full);
        fclose(fd);
    }

    fd = fopen(BAT_NOW_FILE, "r");
    if(fd == NULL) {
        fprintf(stderr, "Error opening energy_now.\n");
        return -1;
    }
    fscanf(fd, "%d", &energy_now);
    fclose(fd);

    return min(((float)energy_now  / (float)energy_full) * 100, 100);
}

/**
 * Return 0 if the battery is discharing, 1 if it's full or is
 * charging, -1 if an error occured.
 **/
int
get_battery_status()
{
    FILE *fd;
    char first_letter;

    if( NULL == (fd = fopen(BAT_STATUS_FILE, "r")))
        return -1;

    fread(&first_letter, sizeof(char), 1, fd);
    fclose(fd);

    if(first_letter == 'D')
        return 0;

    return 1;
}

int
get_mem_percent()
{
    FILE *fd;
    int mem_total;
    int mem_free;
    int mem_available;
    fd = fopen(MEMINFO_FILE, "r");
    if(fd == NULL) {
        fprintf(stderr, "Error opening energy_full.\n");
        return -1;
    }
    fscanf(fd, "MemTotal:%*[ ]%d kB\nMemFree:%*[ ]%d kB\nMemAvailable:%*[ ]%d", &mem_total, &mem_free, &mem_available);
    fclose (fd);
    return ((float)(mem_total-mem_available)/(float)mem_total) * 100;
}


void
get_cpu_usage(int* cpu_percent)
{
    size_t len = 0;
    char *line = NULL;
    int i;
    long int idle_time, other_time;
    char cpu_name[8];

    static int new_cpu_usage[CPU_NBR][4];
    static int old_cpu_usage[CPU_NBR][4];

    FILE *f;
    if(NULL == (f = fopen("/proc/stat", "r")))
        return;

    for(i = 0; i < CPU_NBR; ++i)
    {
        getline(&line,&len,f);
        sscanf(
            line,
            "%s %d %d %d %d",
            cpu_name,
            &new_cpu_usage[i][0],
            &new_cpu_usage[i][1],
            &new_cpu_usage[i][2],
            &new_cpu_usage[i][3]
            );

        if(line != NULL)
        {
            free(line);
            line = NULL;
        }

        idle_time = new_cpu_usage[i][3] - old_cpu_usage[i][3];
        other_time = new_cpu_usage[i][0] - old_cpu_usage[i][0]
            + new_cpu_usage[i][1] - old_cpu_usage[i][1]
            + new_cpu_usage[i][2] - old_cpu_usage[i][2];

        if(idle_time + other_time != 0)
            cpu_percent[i] = (other_time*100)/(idle_time + other_time);
        else
            cpu_percent[i] = 0;

        old_cpu_usage[i][0] = new_cpu_usage[i][0];
        old_cpu_usage[i][1] = new_cpu_usage[i][1];
        old_cpu_usage[i][2] = new_cpu_usage[i][2];
        old_cpu_usage[i][3] = new_cpu_usage[i][3];
    }

    fclose(f);
}

float
get_freq(char *file)
{
    FILE *fd;
    char *freq;
    float ret;

    freq = (char*) malloc(10);
    fd = fopen(file, "r");
    if(fd == NULL)
    {
        fprintf(stderr, "Cannot open '%s' for reading.\n", file);
        exit(1);
    }

    fgets(freq, 10, fd);
    fclose(fd);

    ret = atof(freq)/1000000;
    free(freq);
    return ret;
}

char *
get_date_time()
{
    static char buf[65];
    time_t result;
    struct tm *resulttm;

    result = time(NULL);
    resulttm = localtime(&result);
    if(resulttm == NULL)
    {
        fprintf(stderr, "Error getting localtime.\n");
        exit(1);
    }

    if(!strftime(buf, sizeof(char)*65-1, "[%a %b %d] [%H:%M:%S]", resulttm))
    {
        fprintf(stderr, "strftime is 0.\n");
        exit(1);
    }

    return buf;
}

int
find_sensor()
{
    for(int i = 0; i < 256; i++)
    {
        char fn[256];
        char name[256];
        sprintf(fn, NAME_SENSOR_FILE, i);
        FILE *fd = fopen(fn, "r");
        if (fd == NULL)
            return -1;
        fscanf(fd, "%s", name);
        fclose(fd);
        if (strcmp(name, SENSOR_NAME) == 0)
            return i;
    }
    return -1;
}

int
get_temperature(char *fn)
{
    int temp;
    FILE *fd = fopen(fn, "r");
    if(fd == NULL)
    {
        fprintf(stderr, "Error opening %s.\n", fn);
        return -1;
    }
    fscanf(fd, "%d", &temp);
    fclose(fd);

    return temp / 1000;
}

int
get_wifi_percent()
{
    //size_t len = 0;
    int percent = 0;
    //char line[512] = {'\n'};
    FILE *fd = fopen("/proc/net/wireless", "r");
    if(fd == NULL)
    {
        fprintf(stderr, "Error opening wireless info");
        return -1;
    }
    fscanf(fd, "%*[^\n]\n%*[^\n]\n%*s %*[0-9] %d", &percent);
    fclose(fd);
    return percent;
}

char *
get_wifi_essid()
{
    static char id[IW_ESSID_MAX_SIZE+1];
    int sockfd;
    struct iwreq wreq;

    memset(&wreq, 0, sizeof(struct iwreq));
    wreq.u.essid.length = IW_ESSID_MAX_SIZE+1;
    if (snprintf(wreq.ifr_name, sizeof(wreq.ifr_name), "%s",
                  WLAN_INTERFACE) < 0) {
        return NULL;
    }

    if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
        warn("socket 'AF_INET':");
        return NULL;
    }
    wreq.u.essid.pointer = id;
    if (ioctl(sockfd,SIOCGIWESSID, &wreq) < 0) {
        warn("ioctl 'SIOCGIWESSID':");
        close(sockfd);
        return NULL;
    }

    close(sockfd);

    if (!strcmp(id, "")) {
        return NULL;
    }

    return id;
}

int
get_volume()
{
    const char* MIXER = "Master";
    /* OSS
       const char* OSSMIXCMD = "ossmix vmix0-outvol";
       const char* OSSMIXFORMAT = "Value of mixer control vmix0-outvol is currently set to %f (dB)";
    */

    float vol = 0;
    long pmin, pmax, pvol;

    /* Alsa {{{ */
    snd_mixer_t *handle;
    snd_mixer_selem_id_t *sid;
    snd_mixer_elem_t *elem;
    snd_mixer_selem_id_alloca(&sid);

    if(snd_mixer_open(&handle, 0) < 0)
        return 0;

    if(snd_mixer_attach(handle, "default") < 0
       || snd_mixer_selem_register(handle, NULL, NULL) < 0
       || snd_mixer_load(handle) > 0)
    {
        snd_mixer_close(handle);
        return 0;
    }

    for(elem = snd_mixer_first_elem(handle); elem; elem = snd_mixer_elem_next(elem))
    {
        snd_mixer_selem_get_id(elem, sid);
        if(!strcmp(snd_mixer_selem_id_get_name(sid), MIXER))
        {
            snd_mixer_selem_get_playback_volume_range(elem, &pmin, &pmax);
            snd_mixer_selem_get_playback_volume(elem, SND_MIXER_SCHN_MONO, &pvol);
            vol = ((float)pvol / (float)(pmax - pmin)) * 100;
        }
    }

    snd_mixer_close(handle);
    /* }}} */
    /* Oss (soundcard.h not working) {{{
       if(!(f = popen(OSSMIXCMD, "r")))
       return;

       fscanf(f, OSSMIXFORMAT, &vol);
       pclose(f);
       }}} */

    return vol;
}

